//
//  ShadowView.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 25.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class ShadowView: UIView {
    
    override var bounds: CGRect {
        didSet {
            layer.cornerRadius = 8
            layer.shadowColor = UIColor.lightGray.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowRadius = 4
            layer.shadowOpacity = 1
            layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 8).cgPath
            layer.shouldRasterize = true
            layer.rasterizationScale = UIScreen.main.scale
        }
    }
}
