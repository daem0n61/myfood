//
//  PropertiesTableViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 17.01.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class PropertiesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var propertiesCollectionView1: UICollectionView! {
        didSet {
            propertiesCollectionView1.dataSource = self
            propertiesCollectionView1.delegate = self
        }
    }
    @IBOutlet weak var propertiesCollectionView2: UICollectionView! {
        didSet {
            propertiesCollectionView2.dataSource = self
            propertiesCollectionView2.delegate = self
        }
    }
    
    let properties1 = ["Best1", "Best2", "Best3", "Best4", "Best5"]
    let properties2 = ["Best1", "Best2", "Best3", "Best4", "Best5"]
}

extension PropertiesTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return properties1.count
        }
        return properties2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PropertyCollectionViewCell1", for: indexPath) as! PropertyCollectionViewCell
            //cell.imageView.image = UIImage(named: properties1[indexPath.row])
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PropertyCollectionViewCell2", for: indexPath) as! PropertyCollectionViewCell
        //cell.imageView.image = UIImage(named: properties2[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1 && indexPath.row == 0 {
            return CGSize(width: 150, height: 64)
        }
        return CGSize(width: 64, height: 64)
    }
}
