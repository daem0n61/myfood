//
//  GalleryTableViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 17.01.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class GalleryTableViewCell: UITableViewCell {
    
    var delegate: OrganizationTableViewController!
    
    @IBOutlet weak var galleryCollectionView: UICollectionView! {
        didSet {
            galleryCollectionView.dataSource = self
            galleryCollectionView.delegate = self
        }
    }
    
    let images = ["Best1", "Best2", "Best3", "Best4", "Best5"]
}

extension GalleryTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryPreviewCollectionViewCell", for: indexPath) as! GalleryPreviewCollectionViewCell
        cell.imageView.image = UIImage(named: images[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.performSegue(withIdentifier: "OrganizationToFullscreenGallery", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 270, height: 180)
    }
}
