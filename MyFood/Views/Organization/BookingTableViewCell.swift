//
//  BookingTableViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 21.12.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class BookingTableViewCell: UITableViewCell {
    
    override func layoutSubviews() {
        self.roundedCorners(radius: 16, by: [.topLeft, .topRight])
    }
}
