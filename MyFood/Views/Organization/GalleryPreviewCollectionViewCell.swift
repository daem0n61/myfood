//
//  GalleryPreviewCollectionViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 17.01.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class GalleryPreviewCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func layoutSubviews() {
        self.roundedCorners(radius: 16, by: .allCorners)
    }
}
