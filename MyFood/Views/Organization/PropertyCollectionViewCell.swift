//
//  PropertyCollectionViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 17.01.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class PropertyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func layoutSubviews() {
        self.roundedCornersAndShadow(radius: 32)
    }
}
