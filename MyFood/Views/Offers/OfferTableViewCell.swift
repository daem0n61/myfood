//
//  OfferTableViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 26.12.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var darkView: UIView!
    
    override func layoutSubviews() {
        mainImageView.roundedCorners(radius: 16, by: .allCorners)
        topLabel.layer.cornerRadius = 8
        topLabel.layer.masksToBounds = true
        darkView.roundedCorners(radius: 16, by: .allCorners)
    }
}
