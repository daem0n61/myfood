//
//  FullscreenGalleryCollectionViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 11.02.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class FullscreenGalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewWidth: NSLayoutConstraint!
}
