//
//  GalleryCollectionViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 06.02.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewWidth: NSLayoutConstraint!
}
