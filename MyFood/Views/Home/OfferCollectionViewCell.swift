//
//  OfferCollectionViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 25.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class OfferCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func layoutSubviews() {
        self.layer.cornerRadius = 8
    }
}
