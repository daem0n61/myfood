//
//  HeaderTableViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 24.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    
    override func layoutSubviews() {
        self.roundedCorners(radius: 16, by: [.topLeft, .topRight])
    }
}
