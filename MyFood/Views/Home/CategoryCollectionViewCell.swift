//
//  CategoryCollectionViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 25.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func layoutSubviews() {
        self.roundedCornersAndShadow()
    }
    
    func configure(category: String) {
        
    }
}
