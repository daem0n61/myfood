//
//  OrderCollectionViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 25.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class OrderCollectionViewCell: UICollectionViewCell {
    
    override func layoutSubviews() {
        self.roundedCornersAndShadow()
    }
}
