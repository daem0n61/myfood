//
//  OrganizationWithRatingTableViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 25.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class OrganizationWithRatingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    override func layoutSubviews() {
        logoImageView.roundedCorners(radius: 8, by: [.topLeft, .bottomLeft])
    }
}
