//
//  OrganizationWithoutRatingTableViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 25.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class OrganizationWithoutRatingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func layoutSubviews() {
        logoImageView.roundedCorners(radius: 8, by: [.topLeft, .bottomLeft])
        ratingLabel.layer.cornerRadius = 4
        ratingLabel.layer.masksToBounds = true
    }
}
