//
//  OrdersAndCategoriesTableViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 24.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class OrdersAndCategoriesTableViewCell: UITableViewCell {
    
    private let ordersNumber = 2
    
    var categories = [String]()
    
    @IBOutlet weak var ordersAndCategoriesCollectionView: UICollectionView! {
        didSet {
            ordersAndCategoriesCollectionView.dataSource = self
            ordersAndCategoriesCollectionView.delegate = self
        }
    }
    
    override func layoutSubviews() {
        self.roundedCorners(radius: 16, by: [.topLeft, .topRight])
    }
}

extension OrdersAndCategoriesTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ordersNumber + categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row < ordersNumber {
            return collectionView.dequeueReusableCell(withReuseIdentifier: "OrderCollectionViewCell", for: indexPath)
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
        cell.configure(category: categories[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row < ordersNumber {
            return CGSize(width: 120, height: 90)
        }
        return CGSize(width: 90, height: 90)
    }
}
