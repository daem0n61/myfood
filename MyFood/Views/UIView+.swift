//
//  Extensions.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 25.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

extension UIView {
    
    func roundedCorners(radius: CGFloat, by corners: UIRectCorner) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let layer = CAShapeLayer()
        layer.frame = bounds
        layer.path = path.cgPath
        self.layer.mask = layer
    }
    
    func roundedCornersAndShadow(radius: CGFloat = 8) {
        layer.cornerRadius = 8
        layer.masksToBounds = true
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = 4
        layer.shadowOpacity = 1
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}
