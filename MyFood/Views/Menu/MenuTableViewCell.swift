//
//  MenuTableViewCell.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 07.01.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
