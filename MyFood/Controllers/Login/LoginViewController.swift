//
//  LoginViewController.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 26.12.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField! {
        didSet {
            emailTextField.attributedPlaceholder = NSAttributedString(string: "Электронная почта", attributes: [.font: UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.white])
        }
    }
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "Пароль", attributes: [.font: UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.white])
        }
    }
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var skipButton: UIButton! {
        didSet {
            skipButton.setAttributedTitle(NSAttributedString(string: "Пропустить этот шаг", attributes: [.font: UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.white, .underlineStyle: NSUnderlineStyle.single.rawValue]), for: .normal)
        }
    }
    
    // MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        emailView.layer.cornerRadius = 16
        emailView.layer.masksToBounds = true
        passwordView.layer.cornerRadius = 16
        passwordView.layer.masksToBounds = true
        loginButton.layer.cornerRadius = 16
    }
    
    // MARK: - IBActions
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        let vc = MainViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
