//
//  FullscreenGalleryCollectionViewController.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 10.02.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class FullscreenGalleryCollectionViewController: UICollectionViewController {
    
    let images = ["Best1", "Best2", "Best3", "Best4", "Best5"]

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Collection view data source
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FullscreenGalleryCollectionViewCell", for: indexPath) as! FullscreenGalleryCollectionViewCell
        cell.imageView.image = UIImage(named: images[indexPath.row])
        cell.imageViewWidth.constant = UIScreen.main.bounds.width
        
        return cell
    }
}

extension FullscreenGalleryCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        return CGSize(width: width, height: width)
    }
}
