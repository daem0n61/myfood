//
//  GalleryCollectionViewController.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 06.02.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class GalleryCollectionViewController: UICollectionViewController {
    
    let count = 6

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Collection view data source
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as? GalleryCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.imageView.image = #imageLiteral(resourceName: "Order2")
        return cell
    }
}

extension GalleryCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds.width
        let minSize = (screenSize - 48) / 3
        return CGSize(width: minSize, height: minSize)
    }
}
