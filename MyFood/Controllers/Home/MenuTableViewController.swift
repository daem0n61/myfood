//
//  MenuTableViewController.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 07.01.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    
    let menuItems = ["Мои заказы", "Избранное", "Корзина", "Акции", "Разделитель", "Поддержка", "О сервисе"]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if menuItems[indexPath.row] == "Разделитель" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SeparatorTableViewCell", for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
            cell.titleLabel.text = menuItems[indexPath.row]
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if menuItems[indexPath.row] == "Разделитель" {
            return 100
        } else {
            return 50
        }
    }
}
