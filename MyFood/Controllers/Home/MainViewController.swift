//
//  MainViewController.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 15.01.2020.
//  Copyright © 2020 T-REX Studio. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    var menuController: MenuTableViewController!
    var navController: UINavigationController!
    var isExpanded = false
    
    // MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        if let navVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavigationViewController") as? UINavigationController {
            self.view.addSubview(navVC.view)
            self.addChild(navVC)
            navVC.didMove(toParent: self)
            if let homeVC = navVC.viewControllers.first as? HomeTableViewController {
                homeVC.delegate = self
            }
            navController = navVC
        }
    }
    
    func menuToggle() {
        if menuController == nil {
            if let menuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuTableViewController") as? MenuTableViewController {
                self.view.insertSubview(menuVC.view, at: 0)
                self.addChild(menuVC)
                menuVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 80, height: UIScreen.main.bounds.height)
                menuController = menuVC
            }
        }
        
        isExpanded.toggle()
        
        if isExpanded {
            UIView.animate(withDuration: 0.5) {
                self.navController.view.frame.origin.x = UIScreen.main.bounds.width - 80
            }
        } else {
            UIView.animate(withDuration: 0.5) {
                self.navController.view.frame.origin.x = 0
            }
        }
    }
}
