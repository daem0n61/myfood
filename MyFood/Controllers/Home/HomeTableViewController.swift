//
//  HomeTableViewController.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 24.11.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class HomeTableViewController: UITableViewController {
    
    private var usersCity = "Ростове-на-Дону"
    
    private var searchController: UISearchController {
        let searchController = UISearchController()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск в \(usersCity)"
        searchController.searchBar.tintColor = .black
        searchController.searchBar.setValue("Отмена", forKey: "CancelButtonText")
        return searchController
    }
    
    private var categories = [Category]()
    private var restoraunst = ["Pizza hut", "New York", "Luka Pizza", "Burger King", "Dominos Pizza"]
    private var filteredRestoraunts = [String]()
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return true }
        return text.isEmpty
    }
    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    var menuController: MenuTableViewController!
    var delegate: MainViewController?
    
    // MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.shadowImage = UIImage()

        navigationItem.searchController = searchController
        
        definesPresentationContext = true
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if isFiltering {
            return 1
        } else {
            return 4
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredRestoraunts.count
        } else {
            switch section {
            case 0:
                return 3
            case 1:
                return 4
            case 2:
                return 2
            case 3:
                return 6
            default:
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFiltering {
            let cell = UITableViewCell()
            cell.textLabel?.text = filteredRestoraunts[indexPath.row]
            return cell
        } else {
            switch indexPath.section {
            case 0:
                switch indexPath.row {
                case 0:
                    return tableView.dequeueReusableCell(withIdentifier: "OrdersAndCategoriesTableViewCell")!
                case 1:
                    return tableView.dequeueReusableCell(withIdentifier: "OffersHeaderTableViewCell")!
                case 2:
                    return tableView.dequeueReusableCell(withIdentifier: "OffersTableViewCell")!
                default:
                    return UITableViewCell()
                }
            case 1:
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
                    cell.headerLabel.text = "Закажи завтрак или обед"
                    return cell
                case 3:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "OrganizationWithoutRatingTableViewCell", for: indexPath) as! OrganizationWithoutRatingTableViewCell
                    cell.logoImageView.image = UIImage(named: "Order\(indexPath.row)")
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "OrganizationWithRatingTableViewCell", for: indexPath) as! OrganizationWithRatingTableViewCell
                    cell.logoImageView.image = UIImage(named: "Order\(indexPath.row)")
                    return cell
                }
            case 2:
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
                    cell.headerLabel.text = "Подборки дня"
                    return cell
                }
                return tableView.dequeueReusableCell(withIdentifier: "DigestTableViewCell")!
            case 3:
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
                    cell.headerLabel.text = "Лучшие заведения города"
                    return cell
                case 4:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "OrganizationWithoutRatingTableViewCell", for: indexPath) as! OrganizationWithoutRatingTableViewCell
                    cell.logoImageView.image = UIImage(named: "Best\(indexPath.row)")
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "OrganizationWithRatingTableViewCell", for: indexPath) as! OrganizationWithRatingTableViewCell
                    cell.logoImageView.image = UIImage(named: "Best\(indexPath.row)")
                    return cell
                }
            default:
                return UITableViewCell()
            }
        }
    }
    
    //MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFiltering {
            print(filteredRestoraunts[indexPath.row])
        } else {
            switch indexPath.section {
            case 0:
                if indexPath.row == 1 {
                    performSegue(withIdentifier: "MainToOffers", sender: nil)
                }
            case 1:
                if indexPath.row > 0 {
                    performSegue(withIdentifier: "MainToOrganization", sender: nil)
                }
            case 3:
                if indexPath.row > 0 {
                    performSegue(withIdentifier: "MainToOrganization", sender: nil)
                }
            default:
                break
            }
        }
    }
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        delegate?.menuToggle()
    }
}

extension HomeTableViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredRestoraunts = restoraunst.filter({ (name: String) -> Bool in
            return name.lowercased().contains(searchController.searchBar.text!.lowercased())
        })
        tableView.reloadData()
    }
}
