//
//  OffersTableViewController.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 26.12.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class OffersTableViewController: UITableViewController {
    
    // MARK: - Lifecycle method

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = .black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.tintColor = .black
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfferTableViewCell", for: indexPath)
        return cell
    }
}
