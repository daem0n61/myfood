//
//  OrganizationTableViewController.swift
//  MyFood
//
//  Created by Дмитрий Жданов on 20.12.2019.
//  Copyright © 2019 T-REX Studio. All rights reserved.
//

import UIKit

class OrganizationTableViewController: UITableViewController {
    
    // MARK: - Lifecycle method
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.navigationBar.barStyle = .black
        navigationController!.navigationBar.tintColor = .white
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController!.navigationBar.barStyle = .default
        navigationController!.navigationBar.tintColor = .black
        navigationController!.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 6 {
            return 5
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTableViewCell", for: indexPath)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MainInfoTableViewCell", for: indexPath)
            return cell
        case 2:
            return tableView.dequeueReusableCell(withIdentifier: "GalleryHeaderTableViewCell", for: indexPath)
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "GalleryTableViewCell", for: indexPath) as! GalleryTableViewCell
            cell.delegate = self
            return cell
        case 4:
            return tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell", for: indexPath)
        case 5:
            return tableView.dequeueReusableCell(withIdentifier: "PropertiesTableViewCell", for: indexPath)
        case 6:
            if indexPath.row == 0 {
                return tableView.dequeueReusableCell(withIdentifier: "FeedbackHeaderTableViewCell", for: indexPath)
            }
            return tableView.dequeueReusableCell(withIdentifier: "FeedbackTableViewCell", for: indexPath)
        default:
            return UITableViewCell()
        }
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:
            performSegue(withIdentifier: "OrganizationToGallery", sender: nil)
        case 3:
            print(indexPath)
        default:
            break
        }
    }
}
